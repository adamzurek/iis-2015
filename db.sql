-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `firma` (
  `ico` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`ico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `firma` (`ico`, `nazev`, `heslo`, `aktivni`) VALUES
(12365478,	'Keforolba a.s.',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	0),
(12378945,	'1iT',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	0),
(15975325,	'Kefovači',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	0),
(123456789,	'startup Amatéri s.r.o.',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
(321456789,	'TEST flákači s.r.o',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
(456123789,	'Databázové problémy a.s.',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1);

CREATE TABLE `lekce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kurz` (`kurz`),
  CONSTRAINT `lekce_ibfk_1` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `lekce` (`id`, `nazev`, `popis`, `kurz`) VALUES
(1,	'XOS1',	'Naučte se databázi s OS.',	1),
(2,	'XOS2',	'Pokračování kurzu XOS1.',	1),
(3,	'XiT',	'Stahujeme hudbu. Legálně.',	2),
(4,	'XPY',	'Baví vás skriptování? Zkuste Python u nás',	3),
(5,	'XPN',	'Python? Střelba do nohy.',	4),
(6,	'XRU',	'Baví vás skriptování? Zkuste Ruby u nás',	3);

CREATE TABLE `mistnost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kapacita` int(11) NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `adresa` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `mistnost` (`id`, `kapacita`, `cena`, `adresa`) VALUES
(1,	50,	2000.00,	'Brno, Havajská 8'),
(2,	10,	1000.00,	'Brno, Havajská 7'),
(3,	60,	2500.00,	'Brno, Havajská 7'),
(4,	60,	10000.00,	'Brno, Havajská 7'),
(5,	20,	5000.00,	'Brno, Havajská 5');

DELIMITER ;;

CREATE TRIGGER `trigger_mistnost_insert` BEFORE INSERT ON `mistnost` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.kapacita <= 0
  THEN
    SET msg = 'Error: Capacity must be bigger than 0.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  ELSEIF NEW.cena < 0
    THEN
      SET msg = 'Error: Price must be positive number.';
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_mistnost_update` BEFORE UPDATE ON `mistnost` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.kapacita <= 0
  THEN
    SET msg = 'Error: Capacity must be bigger than 0.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  ELSEIF NEW.cena < 0
    THEN
      SET msg = 'Error: Price must be positive number.';
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

CREATE TABLE `objednane_kurzy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kapacita` int(11) NOT NULL,
  `vede` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) DEFAULT NULL,
  `misto` int(11) NOT NULL,
  `firma` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kurz` (`kurz`),
  KEY `misto` (`misto`),
  KEY `firma` (`firma`),
  KEY `kapacita` (`kapacita`),
  KEY `vede` (`vede`),
  CONSTRAINT `objednane_kurzy_ibfk_1` FOREIGN KEY (`vede`) REFERENCES `zamestnanec` (`rc`) ON UPDATE CASCADE,
  CONSTRAINT `objednane_kurzy_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`),
  CONSTRAINT `objednane_kurzy_ibfk_3` FOREIGN KEY (`misto`) REFERENCES `mistnost` (`id`),
  CONSTRAINT `objednane_kurzy_ibfk_4` FOREIGN KEY (`firma`) REFERENCES `firma` (`ico`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `objednane_kurzy` (`id`, `kapacita`, `vede`, `kurz`, `misto`, `firma`) VALUES
(1,	10,	'9005049878',	1,	5,	123456789),
(2,	50,	'9704030303',	2,	3,	456123789),
(3,	20,	'6509169997',	3,	3,	321456789),
(4,	40,	'9005049878',	1,	1,	123456789),
(5,	40,	'9704030303',	1,	1,	NULL),
(6,	5,	'9005049878',	3,	2,	NULL),
(7,	5,	'7611047719',	1,	2,	123456789);

DELIMITER ;;

CREATE TRIGGER `trigger_objkurzy_insert` BEFORE INSERT ON `objednane_kurzy` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.kapacita <= 0
  THEN
    SET msg = 'Error: Capacity must be bigger than 0.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_objkurzy_update` BEFORE UPDATE ON `objednane_kurzy` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.kapacita <= 0
  THEN
    SET msg = 'Error: Capacity must be bigger than 0.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

CREATE TABLE `objednane_lekce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lekce` int(11) NOT NULL,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lekce_kurz` (`lekce`,`kurz`),
  KEY `lekce` (`lekce`),
  KEY `kurz` (`kurz`),
  CONSTRAINT `objednane_lekce_ibfk_1` FOREIGN KEY (`lekce`) REFERENCES `lekce` (`id`),
  CONSTRAINT `objednane_lekce_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `objednane_kurzy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `objednane_lekce` (`id`, `datum`, `lekce`, `kurz`) VALUES
(1,	'2016-12-01 08:45:00',	1,	1),
(3,	'2016-11-20 11:00:00',	2,	2),
(4,	'2016-01-15 13:30:00',	3,	3),
(5,	'2015-12-31 06:30:00',	2,	1),
(6,	'2015-12-31 16:00:00',	1,	5),
(7,	'2015-12-15 10:00:00',	2,	5);

DELIMITER ;;

CREATE TRIGGER `trigger_objlekce_insert` BEFORE INSERT ON `objednane_lekce` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.datum < NOW()
  THEN
    SET msg = 'Error: Date cannot be earlier than current date.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_objlekce_update` BEFORE UPDATE ON `objednane_lekce` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.datum < NOW()
  THEN
    SET msg = 'Error: Date cannot be earlier than current date.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

CREATE TABLE `prerekvizity` (
  `prerekvizita` int(11) NOT NULL,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`prerekvizita`,`kurz`),
  KEY `kurz` (`kurz`),
  CONSTRAINT `prerekvizity_ibfk_1` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`) ON DELETE CASCADE,
  CONSTRAINT `prerekvizity_ibfk_2` FOREIGN KEY (`prerekvizita`) REFERENCES `provadene_kurzy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `prerekvizity` (`prerekvizita`, `kurz`) VALUES
(1,	2),
(2,	3),
(3,	4),
(1,	6),
(7,	18);

CREATE TABLE `prihlaseni` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`kurz`,`rc`),
  KEY `prihlaseni_ibfk_1` (`rc`),
  CONSTRAINT `prihlaseni_ibfk_1` FOREIGN KEY (`rc`) REFERENCES `ucastnik` (`rc`) ON UPDATE CASCADE,
  CONSTRAINT `prihlaseni_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `objednane_kurzy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `prihlaseni` (`rc`, `kurz`) VALUES
('9209156539',	1),
('9209156539',	2),
('9209156539',	5),
('9209156539',	6),
('9304194163',	1),
('9304194163',	2),
('9304194163',	3),
('9307161237',	2),
('9410283344',	1),
('9502173461',	1),
('9502173461',	2);

CREATE TABLE `provadene_kurzy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `uroven` int(11) NOT NULL,
  `obtiznost` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `provadene_kurzy` (`id`, `nazev`, `uroven`, `obtiznost`, `cena`, `popis`) VALUES
(1,	'Zapíname iOS',	3,	'Pokročilý',	1890.00,	'Jak roztřídit svoje data.'),
(2,	'iTunes a jeho databáze',	5,	'Expert',	7990.00,	'Jak si stáhnout svou hudbu do jabka.'),
(3,	'IDE nebo texťák?',	1,	'Začátečník',	2699.00,	'Poznejte své pro a proti.'),
(4,	'Python není jen had',	1,	'Začátečník',	1399.00,	'Jak se popasovat se svým pythonem.'),
(5,	'Jak (ne)být zlý šéf',	4,	'Mírně pokročilý',	2399.00,	'Štvou vás lidi pod vámi? Řekněte jim to jinak.'),
(6,	'Ruby',	3,	'Pokročilý',	4899.00,	'Ruby, Ruby, Ruby, aaaaa.'),
(7,	'Kreslení s Honzou',	5,	'Expert',	7990.00,	'Když už nevíte jak. A proč.'),
(18,	'Čtverečky a obdélníky',	4,	'Mírně pokročilý',	5199.00,	'Jste vysílení, ale odhodlaní kreslit dál. Honza je králem.'),
(19,	'Svačíme v noci',	2,	'Pokročilý začátečník',	3790.00,	'Co dělat když vás v noci propadne hlad a vy musíte pracovat');

DELIMITER ;;

CREATE TRIGGER `trigger_prkuzy_insert` BEFORE INSERT ON `provadene_kurzy` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.cena < 0
  THEN
    SET msg = 'Error: Price must be positive number.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  ELSEIF NEW.uroven <= 0 OR NEW.uroven > 10
    THEN
      SET msg = 'Error: Level must be bigger than 0 and smaller than 11.';
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_prkurzy_update` BEFORE UPDATE ON `provadene_kurzy` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF NEW.cena < 0
  THEN
    SET msg = 'Error: Price must be positive number.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  ELSEIF NEW.uroven <= 0 OR NEW.uroven > 10
    THEN
      SET msg = 'Error: Level must be bigger than 0 and smaller than 11.';
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

CREATE TABLE `stredisko` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `stredisko` (`rc`, `jmeno`, `heslo`, `aktivni`) VALUES
('9005049878',	'Dorota Zavisova',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9405303369',	'Michal Cokoladovy',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9704030303',	'Adolf Most',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1);

CREATE TABLE `ucastnik` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `adresa` varchar(60) COLLATE utf8_czech_ci DEFAULT NULL,
  `zamestnan` int(11) DEFAULT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`),
  KEY `ucastnik_ibfk_1` (`zamestnan`),
  CONSTRAINT `ucastnik_ibfk_1` FOREIGN KEY (`zamestnan`) REFERENCES `firma` (`ico`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `ucastnik` (`rc`, `jmeno`, `adresa`, `zamestnan`, `heslo`, `aktivni`) VALUES
('9108265980',	'Michal',	'purkynova',	123456789,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9209156539',	'Jan Travolta',	'Zlín 559',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9304194163',	'Matus Hruskovica',	'Liptovský Mikuláš, Osová 12',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9307161237',	'Justin Drevojazero',	'Gajary 57',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9307163250',	'Adam Žurek',	'Brno 1234',	12365478,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9307163261',	'Jan Tráva',	'Brno 123',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9354196412',	'Helena Podkovickova',	'',	321456789,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9410283344',	'Nikola Dietachlapova',	'Brno, Ponava 70',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9502173461',	'Magdalena Liskova',	'Břeclav, Nemocničná 4 ',	NULL,	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1);

DELIMITER ;;

CREATE TRIGGER `trigger_ucastnik_insert` BEFORE INSERT ON `ucastnik` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF MOD(NEW.rc, 11) != 0
  THEN
    SET msg = 'Error: RC is invalid.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_ucastnik_update` BEFORE UPDATE ON `ucastnik` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF MOD(NEW.rc, 11) != 0
  THEN
    SET msg = 'Error: RC is invalid.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

CREATE TABLE `vyskoleni` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) NOT NULL,
  KEY `kurz` (`kurz`),
  KEY `vyskoleni_ibfk_1` (`rc`),
  CONSTRAINT `vyskoleni_ibfk_1` FOREIGN KEY (`rc`) REFERENCES `zamestnanec` (`rc`) ON UPDATE CASCADE,
  CONSTRAINT `vyskoleni_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `vyskoleni` (`rc`, `kurz`) VALUES
('9005049878',	1),
('9405303369',	1),
('9704030303',	1),
('9005049878',	2),
('9704030303',	2),
('7611047719',	3),
('9704030303',	3),
('7611047719',	4),
('7611047719',	6),
('6509169997',	5);

CREATE TABLE `zamestnanec` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zamestnanec` (`rc`, `jmeno`, `heslo`, `aktivni`) VALUES
('6509169997',	'David Nemily',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('7611047719',	'Lutrecia Drakulova',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9005049878',	'Dorota Zavisova',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9405303369',	'Michal Cokoladovy',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
('9704030303',	'Adolf Most',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1);

DELIMITER ;;

CREATE TRIGGER `trigger_zamestnanec_insert` BEFORE INSERT ON `zamestnanec` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF MOD(NEW.rc, 11) != 0
  THEN
    SET msg = 'Error: RC is invalid.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

CREATE TRIGGER `trigger_zamestnanec_update` BEFORE UPDATE ON `zamestnanec` FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(255);
  IF MOD(NEW.rc, 11) != 0
  THEN
    SET msg = 'Error: RC is invalid.';
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = msg;
  END IF;
END;;

DELIMITER ;

-- 2015-12-07 22:34:00
