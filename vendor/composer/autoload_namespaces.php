<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'WebLoader' => array($vendorDir . '/janmarek/webloader'),
    'Less' => array($vendorDir . '/oyejorge/less.php/lib'),
);
