<?php

namespace App\Form;

use App\Model\CourseModel;
use App\Components\BootstrapForm as Form;
use Nette\Forms\Controls\BaseControl;

class EmployeeFormFactory
{
    /**
     * @var CourseModel
     */
    private $courseModel;

    /**
     * @param CourseModel $courseModel
     */
    public function __construct (CourseModel $courseModel)
    {
        $this->courseModel = $courseModel;
    }

    public function create ()
    {
        $form = new Form;

        $form->addText('rc', 'Rodné číslo:')
            ->setRequired('Rodné číslo lektora je povinné.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Rodné číslo není celé číslo.')
            ->addRule(Form::MAX_LENGTH, 'Rodné číslo nemá 10 čísel.', 10)
            ->addRule(Form::MIN_LENGTH, 'Rodné číslo nemá 10 čísel.', 10)
            ->addRule(array ($this, 'validateRC'), 'Rodné číslo není dělitelné 11.');

        $form->addText('jmeno', 'Jméno a příjmení:')
            ->setRequired('Jméno a příjmení lektora je povinné.');

        $courses = $this->courseModel
            ->getCourses()
            ->fetchPairs('id', 'nazev');

        $form->addCheckboxList('vyskoleny', 'Vyškolený:')
            ->setItems($courses);

        return $form;
    }

    /**
     * @param BaseControl $control
     *
     * @return bool
     */
    public function validateRC (BaseControl $control)
    {
        $rc = $control->getValue();

        if ($rc % 11 !== 0) {
            $control->addError('Rodné číslo není dělitelné číslem 11.');

            return false;
        }

        return true;
    }
}
