<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;
use App\Model\AttendeeModel;
use App\Model\CompanyModel;
use Nette\Forms\Controls\TextInput;

class AttendeeFormFactory
{
    /**
     * @var AttendeeModel
     */
    private $attendeeModel;

    /**
     * @var CompanyModel
     */
    private $companyModel;

    /**
     * @param AttendeeModel $attendeeModel
     * @param CompanyModel $companyModel
     */
    public function __construct (AttendeeModel $attendeeModel, CompanyModel $companyModel)
    {
        $this->attendeeModel = $attendeeModel;
        $this->companyModel = $companyModel;
    }

    /**
     * @return Form
     */
    public function create ()
    {
        $form = new Form;

        $form->addText('jmeno', 'Jméno a příjmení:')
            ->setRequired('Jméno a příjmení je povinné.');

        $form->addText('rc', 'Rodné číslo:')
            ->setRequired('Rodné číslo je povinné.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Rodné číslo musí být celé číslo.')
            ->addRule(Form::MAX_LENGTH, 'Rodné číslo musí mít 10 čísel.', 10)
            ->addRule(Form::MIN_LENGTH, 'Rodné číslo musí mít 10 čísel.', 10)
            ->addRule(function (TextInput $control) {
                return Validators::validateRC($control);
            }, 'Rodné číslo musí být dělitelné 11.');

        $form->addText('adresa', 'Adresa:');

        $companies = $this->companyModel
            ->getCompanies()
            ->fetchPairs('ico', 'nazev');

        $form->addSelect('zamestnan', 'Firma:')
            ->setItems(array (0 => 'Žádná firma') + $companies);

        $form->addPassword('heslo', 'Heslo:');

        $form->addPassword('confirm_password', 'Heslo pro potvrzení:')
            ->setOmitted()
            ->addConditionOn($form['heslo'], Form::FILLED)
            ->setRequired('Potvrzující heslo je povinné.')
            ->addRule(Form::EQUAL, 'Hesla se musí shodovat!', $form['heslo']);

        return $form;
    }
}
