<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;

class RoomFormFactory
{
    public function create ()
    {
        $form = new Form;

        $form->addText('kapacita', 'Kapacita')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Musí být celé číslo.')
            ->addRule(Form::MIN, 'Musí být větší než 0', 0)
            ->setRequired();

        $form->addText('cena', 'Cena')
            ->setRequired()
            ->setType('number')
            ->addRule(Form::MIN, 'Musí být větší než 0', 0);

        $form->addText('adresa', 'Adresa')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Musí být kratší než 100 znaků.', 100);

        $form->addSubmit('add', 'Přidat');

        return $form;
    }
}
