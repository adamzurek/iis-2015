<?php

namespace App\Form;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Validators as NValidators;

class Validators extends NValidators
{
    /**
     * @param TextInput $control
     *
     * @return bool
     */
    public static function validateRC (TextInput $control)
    {
        $rc = $control->getValue();

        if ($rc % 11 !== 0) {
            $control->addError('Rodné číslo je neplatné. Zkontrolujte si jeho správnost.');

            return false;
        }

        return true;
    }
}
