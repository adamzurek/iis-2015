<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;
use App\Model\CourseModel;

class LectureFormFactory
{
    /**
     * @var CourseModel
     */
    private $courseModel;

    /**
     * @param CourseModel $courseModel
     */
    public function __construct (CourseModel $courseModel)
    {

        $this->courseModel = $courseModel;
    }

    /**
     * @return Form
     */
    public function create ()
    {
        $form = new Form;

        $form->addText('nazev', 'Jméno lekce:')
            ->setRequired('Vložte jméno lekce.');

        $form->addText('popis', 'Popis:');

        $courses = $this->courseModel
            ->getCourses()
            ->fetchPairs('id', 'nazev');

        $form->addSelect('kurz', 'Kurz:')
            ->setItems($courses);

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }
}
