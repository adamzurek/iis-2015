<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;
use App\Model\OrderedLectureModel;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Vodacek\Forms\Controls\DateInput;

class LectureOrderFormFactory
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var OrderedLectureModel
     */
    private $orderedLectureModel;

    /**
     * @param OrderedLectureModel $orderedLectureModel
     * @param User $user
     *
     * @internal param LectureModel $lectureModel
     */
    public function __construct (OrderedLectureModel $orderedLectureModel, User $user)
    {
        $this->user = $user;
        $this->orderedLectureModel = $orderedLectureModel;
    }

    public function create ()
    {
        $form = new Form;

        $form->addHidden('lekce');
        $form->addHidden('kurz');

        $dateInput = new DateInput('Datum:');
        $dateInput->setRequired('Zadejte datum.')
            ->addRule(function (DateInput $control) {
                return ($control->getValue() > new \DateTime);
            }, 'Zadejte pozdější čas.');
        $form->addComponent($dateInput, 'datum');

        $form->addSubmit('submit', in_array('stredisko', $this->user->getRoles(), true) ? 'Vypsat' : 'Objednat');

        return $form;
    }
}
