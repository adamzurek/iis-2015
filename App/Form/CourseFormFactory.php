<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;
use App\Model\CourseModel;

class CourseFormFactory
{
    /**
     * @var CourseModel
     */
    private $courseModel;

    /**
     * @param CourseModel $courseModel
     */
    public function __construct (CourseModel $courseModel)
    {
        $this->courseModel = $courseModel;
    }

    public function create ()
    {
        $form = new Form;

        $form->addText('nazev', 'Jméno kurzu:')
            ->setRequired('Jméno kurzu je povinné.');

        $form->addText('uroven', 'Úroveň:')
            ->setRequired('Úroveň kurzu je povinná.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Úroveň musí být celé číslo.')
            ->addRule(Form::RANGE, 'Úroveň musí být v rozsahu 1 - 10.', array (1, 10));

        $form->addText('obtiznost', 'Obtížnost:')
            ->setRequired('Obtížnost kurzu je povinná.');

        $form->addText('cena', 'Cena:')
            ->setRequired('Cena kurzu je povinná.')
            ->addRule(Form::INTEGER, 'Cena musí být celé číslo.')
            ->addRule(FORM::MAX_LENGTH, 'Maximální délka ceny musí být 12 znaků', 12);

        $form->addText('popis', 'Popis:');

        $prerequisites = $this->courseModel
            ->getCourses()
            ->fetchPairs('id', 'nazev');

        $form->addCheckboxList('prerekvizity', 'Prerekvizity:', $prerequisites);

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }
}
