<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;
use App\Model\LectorModel;
use App\Model\OrderedCourseModel;
use App\Model\RoomModel;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class CourseOrderFormFactory
{
    /**
     * @var RoomModel
     */
    private $roomModel;

    /**
     * @var LectorModel
     */
    private $lectorModel;

    /**
     * @var User
     */
    private $user;

    /**
     * @var OrderedCourseModel
     */
    private $orderedCourseModel;

    /**
     * @param LectorModel $lectorModel
     * @param RoomModel $roomModel
     * @param User $user
     * @param OrderedCourseModel $orderedCourseModel
     */
    public function __construct (LectorModel $lectorModel, RoomModel $roomModel, User $user, OrderedCourseModel $orderedCourseModel)
    {
        $this->lectorModel = $lectorModel;
        $this->roomModel = $roomModel;
        $this->user = $user;
        $this->orderedCourseModel = $orderedCourseModel;
    }

    /**
     * @return Form
     */
    public function create ()
    {
        $rooms = array ();
        foreach ($this->roomModel->getRooms() as $row) {
            $rooms[$row->id] = $row->kapacita . ' míst, ' . $row->adresa . ', ' . $row->cena . ' Kč';
        }

        $form = new Form;

        $form->addRadioList('misto', 'Místnosti:', $rooms)
            ->setRequired('Místnost je povinná.');

        $employees = $this->lectorModel
            ->getLectors()
            ->order('jmeno')
            ->fetchPairs('rc', 'jmeno');

        $form->addSelect('vede', 'Lektor:')
            ->setItems($employees);

        $form->addText('kapacita', 'Kapacita:')
            ->setRequired('Musíte zadat kapacitu kurzu.')
            ->setType('number')
            ->addRule(FORM::INTEGER, 'Kapacita musí být celé číslo.')
            ->addRule(FORM::MIN, 'Kapacita musí být větší jak 0.', 1);

        $form->addHidden('kurz');

        $form->addSubmit('submit', in_array('stredisko', $this->user->getRoles(), true) ? 'Vypsat' : 'Objednat');

        $form->onValidate[] = array ($this, 'courseFormValidate');

        return $form;
    }

    /**
     * @param Form $form
     *
     * @return bool
     */
    public function courseFormValidate (Form $form)
    {
        $values = $form->getValues();

        $room = $this->roomModel->getRoom($values->misto);

        if ($values->kapacita > $room->kapacita) {
            $form['kapacita']->addError('Tam se nevejdete. Ve vybrané místnosti je ' . $room->kapacita . ' míst.');

            return false;
        }

        return true;
    }
}
