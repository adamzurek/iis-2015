<?php

namespace App\Form;

use App\Components\BootstrapForm as Form;

class CompanyFormFactory
{

    public function create()
    {
        $form = new Form;

        $form->addText('ico', 'IČO:')
            ->setRequired('Vložte IČO firmy.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'IČO musí být celé číslo.')
            ->addRule(Form::MAX_LENGTH, 'IČO musí mít 8 čísel', 8)
            ->addRule(Form::MIN_LENGTH, 'IČO musí mít 8 čísel', 8);

        $form->addText('nazev', 'Název firmy:')
            ->setRequired('Vložte název firmy.');

        $form->addPassword('heslo', 'Heslo:');

        $form->addPassword('confirm_password', 'Heslo potvrzení: ')
            ->setOmitted()
            ->addConditionOn($form['heslo'], Form::FILLED)
            ->setRequired('Potvrzující heslo je povinné.')
            ->addRule(Form::EQUAL, 'Hesla se musí shodovat!', $form['heslo']);

        return $form;
    }
}
