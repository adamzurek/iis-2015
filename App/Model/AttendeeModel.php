<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class AttendeeModel extends BaseModel
{
    const TABLE_ENROLLED = 'prihlaseni';

    /**
     * @var string
     */
    protected $table = 'ucastnik';

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getAttendee ($id)
    {
        return $this->getById($id);
    }

    /**
     * @return Selection
     */
    public function getAttendees ()
    {
        return $this->getAll();
    }

    /**
     * @param ArrayHash $data
     *
     * @return bool|int|IRow
     */
    public function addAttendee (ArrayHash $data)
    {
        if (isset($data->nezamestnan)) {
            unset($data->nezamestnan);
        }

        $data->aktivni = 1;

        $data->heslo = sha1($data->heslo);

        return $this->getTable()->insert($data);
    }

    /**
     * @param int $id
     * @param ArrayHash $data
     *
     * @return bool
     */
    public function editAttendee ($id, ArrayHash $data)
    {
        if ($data->zamestnan == 0) {
            $data->zamestnan = null;
        }

        if (!empty($data->rc)) {
            unset($data->rc);
        }

        if (empty($data->heslo)) {
            unset($data->heslo);
        } else {
            $data->heslo = sha1($data->heslo);
        }

        return (bool)$this->getById($id, false)
            ->update($data);
    }

    /**
     * @param int $course_id
     *
     * @return Selection
     */
    public function getAttendeesByCourse ($course_id)
    {
        return $this->getAll()
            ->where(':' . self::TABLE_ENROLLED . '.kurz', $course_id);
    }

    /**
     * @param int $id_attendee
     * @param int $id_course
     *
     * @return bool
     */
    public function hasAttendeeCoursed ($id_attendee, $id_course)
    {
        return (bool)$this->getAttendeesByCourse($id_course)
            ->where(':' . self::TABLE_ENROLLED . '.rc', $id_attendee)
            ->fetch();
    }
}
