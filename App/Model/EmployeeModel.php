<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;

class EmployeeModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'zamestnanec';

    /**
     * @param int $rc
     *
     * @return bool|IRow
     */
    public function getEmployee ($rc)
    {
        return $this->getById($rc);
    }

    /**
     * @return Selection
     */
    public function getEmployees ()
    {
        return $this->getAll();
    }

    /**
     * @param int $rc
     * @param ArrayHash $data
     *
     * @return int
     */
    public function editEmployee ($rc, ArrayHash $data)
    {
        $employee = clone $data;

        unset($employee->vyskoleny);

        $this
            ->getById($rc, false)
            ->update($employee);

        return true;
    }

    /**
     * @param ArrayHash $data
     *
     * @return bool|int|IRow
     */
    public function addEmployee (ArrayHash $data)
    {
        $employee = clone $data;
        unset($employee->vyskoleny);

        $employee['heslo'] = Random::generate(40, '0-9a-z');

        return $this
            ->getTable()
            ->insert($employee);
    }
}
