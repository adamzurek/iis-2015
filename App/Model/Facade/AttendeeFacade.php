<?php

namespace App\Model\Facade;

use App\Components\BootstrapForm as Form;
use App\Form\AttendeeFormFactory;
use App\Model\AttendeeModel;
use App\Model\CompanyModel;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class AttendeeFacade
{
    /**
     * @var AttendeeModel
     */
    private $attendeeModel;

    /**
     * @var AttendeeFormFactory
     */
    private $attendeeFormFactory;

    /**
     * @var CompanyModel
     */
    private $companyModel;

    /**
     * @var User
     */
    private $user;

    /**
     * @param AttendeeModel $attendeeModel
     * @param AttendeeFormFactory $attendeeFormFactory
     * @param CompanyModel $companyModel
     * @param User $user
     */
    public function __construct (AttendeeModel $attendeeModel, AttendeeFormFactory $attendeeFormFactory, CompanyModel $companyModel, User $user)
    {
        $this->attendeeModel = $attendeeModel;
        $this->attendeeFormFactory = $attendeeFormFactory;
        $this->companyModel = $companyModel;
        $this->user = $user;
    }

    /**
     * @return Form
     */
    public function createRegisterForm ()
    {
        $form = $this->attendeeFormFactory->create();

        $form['heslo']->setRequired('Heslo je povinné.');
        $form['confirm_password']->setRequired('Potvrzení hesla je povinné.');

        $form->addSubmit('submit', 'Registrovat');

        $form->onValidate[] = array ($this, 'isUniqueAttendee');

        $attendeeModel = $this->attendeeModel;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($attendeeModel) {
            return (bool)$attendeeModel->addAttendee($values);
        };

        return $form;
    }

    /**
     * @param IRow $attendee
     *
     * @return Form
     */
    public function createEditForm (IRow $attendee)
    {
        $form = $this->attendeeFormFactory->create();

        if ($this->user->isInRole('stredisko')) {
            $form->addCheckbox('aktivni', 'Aktivní');
        }

        $form->setDefaults($attendee);

        $form->addSubmit('edit', 'Editovat');

        $attendeeModel = $this->attendeeModel;
        $user = $this->user;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($attendeeModel, $user) {
            return $attendeeModel->editAttendee($values->rc, $values);
        };

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     *
     * @return bool
     */
    public function isUniqueAttendee (Form $form, ArrayHash $values)
    {
        $exists = $this->attendeeModel->getAttendee($values->rc);

        if ($exists) {
            $form['rc']->addError('Účastník s rodným číslem: ' . $values->rc . ' je již registrován.');

            return false;
        }

        return true;
    }

    /**
     * @param int $rc
     *
     * @return bool|IRow
     */
    public function getAttendee ($rc)
    {
        return $this->attendeeModel->getAttendee($rc);
    }

    /**
     * @return Selection
     */
    public function getAttendees ()
    {
        return $this->attendeeModel->getAttendees();
    }
}
