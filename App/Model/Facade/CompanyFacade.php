<?php

namespace App\Model\Facade;

use App\Components\BootstrapForm as Form;
use App\Form\CompanyFormFactory;
use App\Model\CompanyModel;
use Nette\Database\Table\Selection;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class CompanyFacade
{
    /**
     * @var CompanyModel
     */
    private $companyModel;

    /**
     * @var CompanyFormFactory
     */
    private $companyFormFactory;

    /**
     * @var User
     */
    private $user;

    /**
     * @param CompanyModel $companyModel
     * @param CompanyFormFactory $companyFormFactory
     * @param User $user
     */
    public function __construct (CompanyModel $companyModel, CompanyFormFactory $companyFormFactory, User $user)
    {
        $this->companyModel = $companyModel;
        $this->companyFormFactory = $companyFormFactory;
        $this->user = $user;
    }

    /**
     * @return Form
     */
    public function createRegisterForm ()
    {
        $form = $this->companyFormFactory->create();

        $form->addSubmit('submit', 'Registrovat');

        $form['heslo']->setRequired('Heslo je povinné.');
        $form['confirm_password']->setRequired('Potvrzovací heslo je povinné.');

        $companyModel = $this->companyModel;

        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($companyModel) {
            return (bool)$companyModel->addCompany($values);
        };

        $form->onValidate[] = function (Form $form, ArrayHash $values) use ($companyModel) {
            $company = $companyModel->getCompany($values->ico);

            if ($company) {
                $form['ico']->addError('Firma s IČO: ' . $company->ico . ' je u nás již registrována.');

                return false;
            }

            return true;
        };

        return $form;
    }

    /**
     * @param int $id_company
     *
     * @return Form
     */
    public function createEditForm ($id_company)
    {
        $form = $this->companyFormFactory->create();

        $company = $this->companyModel->getCompany($id_company);

        $form->removeComponent($form['ico']);
        $form->addHidden('ico')
            ->setValue($id_company);

        if ($this->user->isInRole('stredisko')) {
            $form->addCheckbox('aktivni', 'Aktivní');
        }

        $form->setDefaults($company);

        $form->addSubmit('edit', 'Uložit');

        $companyModel = $this->companyModel;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($companyModel) {
            $id = $values->ico;
            unset($values->ico);

            $companyModel->editCompany($id, $values);
        };

        return $form;
    }

    /**
     * @return Selection
     */
    public function getCompanies ()
    {
        return $this->companyModel->getCompanies();
    }
}
