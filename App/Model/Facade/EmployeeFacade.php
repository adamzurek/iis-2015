<?php

namespace App\Model\Facade;

use App\Model\EmployeeModel;
use App\Components\BootstrapForm as Form;
use App\Form\EmployeeFormFactory;
use App\Model\EmployeeEducatedModel;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class EmployeeFacade
{
    /**
     * @var EmployeeModel
     */
    private $employeeModel;

    /**
     * @var EmployeeEducatedModel
     */
    private $employeeEducatedModel;

    /**
     * @var EmployeeFormFactory
     */
    private $employeeFormFactory;

    /**
     * @var User
     */
    private $user;

    /**
     * @param EmployeeModel $employeeModel
     * @param EmployeeEducatedModel $employeeEducatedModel
     * @param EmployeeFormFactory $employeeFormFactory
     * @param User $user
     */
    public function __construct (EmployeeModel $employeeModel, EmployeeEducatedModel $employeeEducatedModel, EmployeeFormFactory $employeeFormFactory, User $user)
    {
        $this->employeeModel = $employeeModel;
        $this->employeeEducatedModel = $employeeEducatedModel;
        $this->employeeFormFactory = $employeeFormFactory;
        $this->user = $user;
    }

    /**
     * @param ArrayHash $data
     *
     * @return bool
     */
    public function addEmployee (ArrayHash $data)
    {
        $rc = $this->employeeModel->addEmployee($data);

        if ($rc) {
            return $this->employeeEducatedModel->editEducation($rc, $data);
        }

        return false;
    }

    /**
     * @param int $rc
     * @param ArrayHash $data
     *
     * @return bool
     */
    public function editEmployee ($rc, ArrayHash $data)
    {
        return $this->employeeModel->editEmployee($rc, $data) && $this->employeeEducatedModel->editEducation($rc, $data);
    }

    /**
     * @param int $rc
     *
     * @return bool|IRow
     */
    public function getEmployee ($rc)
    {
        return $this->employeeModel->getEmployee($rc);
    }

    /**
     * @return Selection
     */
    public function getEmployees ()
    {
        return $this->employeeModel->getEmployees();
    }

    /**
     * @return Form
     */
    public function createRegisterForm ()
    {
        $form = $this->employeeFormFactory->create();

        $form->addSubmit('submit', 'Registrovat');

        $form->onValidate[] = array ($this, 'registerFormValidate');

        $me = $this;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($me) {
            return $me->addEmployee($values);
        };

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     *
     * @return bool
     */
    public function registerFormValidate (Form $form, ArrayHash $values)
    {
        $exists = $this->getEmployee($values->rc);

        if ($exists) {
            $form['rc']->addError('Zaměstnanec s rodným číslem: ' . $values->rc . ' je u nás již registrován.');

            return false;
        }

        return true;
    }

    /**
     * @param IRow $employee
     *
     * @return Form
     */
    public function createEditForm (IRow $employee)
    {
        $form = $this->employeeFormFactory->create();

        if ($this->user->isInRole('stredisko')) {
            $form->addCheckbox('aktivni', 'Aktivní');
        }

        unset($form['rc']);
        $form->addHidden('rc');
        $form->setDefaults($employee);

        $educated = $this->employeeEducatedModel->getEducation($employee->rc);

        $form['vyskoleny']->setDefaultValue($educated->fetchPairs(null, 'kurz'));

        $form->addSubmit('submit', 'Uložit');

        $me = $this;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($me) {
            $rc = $values->rc;
            unset($values->rc);

            return $me->editEmployee($rc, $values);
        };

        return $form;
    }
}
