<?php

namespace App\Model;

use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class EmployeeEducatedModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'vyskoleni';

    /**
     * @param int $rc
     * @param ArrayHash $data
     *
     * @return bool
     */
    public function editEducation ($rc, ArrayHash $data)
    {
        $this
            ->getTable()
            ->where('rc', $rc)
            ->delete();

        foreach ($data->vyskoleny as $course_id) {
            $this
                ->getTable()
                ->insert(array (
                             'rc'   => $rc,
                             'kurz' => $course_id,
                         ));

            $checkState = $this
                ->getTable()
                ->where('rc', $rc)
                ->where('kurz', $course_id)
                ->fetch();

            if (!$checkState) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $id_employee
     *
     * @return Selection
     */
    public function getEducation ($id_employee)
    {
        return $this->getTable()
            ->where('rc', $id_employee);
    }
}
