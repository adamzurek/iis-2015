<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class CompanyModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'firma';

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getCompany ($id)
    {
        return $this->getById($id);
    }

    /**
     * @return Selection
     */
    public function getCompanies ()
    {
        return $this->getAll();
    }

    /**
     * @param ArrayHash $data
     *
     * @return bool|IRow
     */
    public function addCompany (ArrayHash $data)
    {
        $data->heslo = sha1($data->heslo);
        $this->getTable()->insert($data);

        return $this->getById($data->ico);
    }

    /**
     * @param int $id
     * @param ArrayHash $data
     *
     * @return int
     */
    public function editCompany ($id, ArrayHash $data)
    {
        if (empty($data->heslo)) {
            unset($data->heslo);
        } else {
            $data->heslo = sha1($data->heslo);
        }

        return $this
            ->getById($id, false)
            ->update($data);
    }
}
