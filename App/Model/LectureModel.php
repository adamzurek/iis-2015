<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class LectureModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'lekce';

    /**
     * @param int $id
     *
     * @return bool|mixed|IRow
     */
    public function getLecture ($id)
    {
        return $this->getById($id);
    }

    /**
     * @param int|null $course_id
     *
     * @return Selection
     */
    public function getLectures ($course_id = null)
    {
        $lectures = $this->getAll();

        if ($course_id) {
            $lectures = $lectures->where('kurz', $course_id);
        }

        return $lectures;
    }

    /**
     * @param int $lecture_id
     * @param array|ArrayHash $data
     *
     * @return int
     */
    public function editLecture ($lecture_id, $data)
    {
        return $this
            ->getById($lecture_id, false)
            ->update($data);
    }

    /**
     * @param array|ArrayHash $data
     *
     * @return bool
     */
    public function addLecture ($data)
    {
        return (bool)$this
            ->getTable()
            ->insert($data);
    }
}
