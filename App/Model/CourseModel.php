<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class CourseModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'provadene_kurzy';

    const
        TABLE_ORDERED_COURSE = 'objednane_kurzy',
        TABLE_PREREQUISITE = 'prerekvizity';

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getCourse ($id)
    {
        return $this->getById($id);
    }

    /**
     * @return Selection
     */
    public function getCourses ()
    {
        return $this->getAll();
    }

    /**
     * @param int $course_id
     *
     * @return Selection
     */
    public function getPrerequisites ($course_id)
    {
        return $this->db
            ->table(self::TABLE_PREREQUISITE)
            ->where(self::TABLE_PREREQUISITE . '.kurz', $course_id);
    }

    /**
     * @param ArrayHash $data
     *
     * @return bool
     */
    public function addCourse (ArrayHash $data)
    {
        $provadene_kurzy = clone $data;
        unset($provadene_kurzy->prerekvizity);

        $course_id = $this
            ->getTable()
            ->insert($provadene_kurzy);

        if ($course_id) {
            if (isset($data->prerekvizity)) {
                return $this->editPrerequisites($course_id, $data->prerekvizity);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $course_id
     * @param ArrayHash $data
     *
     * @return int
     */
    public function editCourse ($course_id, ArrayHash $data)
    {
        if (isset($data->prerekvizity)) {
            $this->editPrerequisites($course_id, $data->prerekvizity);

            unset($data->prerekvizity);
        }

        return $this
            ->getById($course_id, false)
            ->update($data);
    }

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getOrderedCourseInfo ($id)
    {
        return $this
            ->getTable()
            ->where(':' . self::TABLE_ORDERED_COURSE . '.id', $id)
            ->fetch();
    }

    /**
     * @param int $course_id
     * @param array $prerequisites
     *
     * @return bool
     */
    private function editPrerequisites ($course_id, array $prerequisites)
    {
        $this
            ->getTable(self::TABLE_PREREQUISITE)
            ->where('kurz', $course_id)
            ->delete();

        foreach ($prerequisites as $row) {
            $ok = $this
                ->getTable(self::TABLE_PREREQUISITE)
                ->insert(array ('prerekvizita' => $row,
                                'kurz'         => $course_id,
                         ));

            if (!$ok) {
                return false;
            }
        }
    }
}
