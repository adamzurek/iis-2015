<?php

namespace App\Model;

use Nette\Database\Context as DbContext;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Object;

class BaseModel extends Object
{
    /**
     * @var DbContext
     */
    protected $db;

    /**
     * @param DbContext $db
     */
    public function __construct (DbContext $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $table
     *
     * @return Selection
     */
    protected function getTable ($table = '')
    {
        return $this->db
            ->table($table ?: $this->table);
    }

    /**
     * @param string $table
     *
     * @return Selection
     */
    protected function getAll ($table = '')
    {
        return $this->db
            ->table($table ?: $this->table);
    }

    /**
     * @param int $id
     * @param bool $fetch
     *
     * @return bool|IRow|Selection
     */
    protected function getById ($id, $fetch = true)
    {
        $db = $this
            ->getAll()
            ->wherePrimary($id);

        if ($fetch) {
            return $db->fetch();
        }

        return $db;
    }
}
