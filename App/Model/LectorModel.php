<?php

namespace App\Model;

use Nette\Database\Table\Selection;

class LectorModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'zamestnanec';

    const TABLE_EDUCATED = 'vyskoleni';

    /**
     * @param int|null $course_id
     *
     * @return Selection
     */
    public function getLectors ($course_id = null)
    {
        $db = $this->getAll();

        if ($course_id) {
            $db->where(':' . self::TABLE_EDUCATED . '.kurz', $course_id);
        }

        return $db;
    }
}
