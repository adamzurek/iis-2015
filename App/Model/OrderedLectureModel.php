<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class OrderedLectureModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'objednane_lekce';

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getOrderedLecture ($id)
    {
        return $this
            ->getTable()
            ->wherePrimary($id)
            ->fetch();
    }

    /**
     * @param array|ArrayHash $values
     *
     * @return bool|int|IRow
     */
    public function orderLecture ($values)
    {
        return $this
            ->getTable()
            ->insert($values);
    }

    /**
     * @param int $id_course
     *
     * @return Selection
     */
    public function getOrderedLectures ($id_course)
    {
        return $this
            ->getAll()
            ->where('kurz', $id_course);
    }

    /**
     * @param int $id
     * @param ArrayHash $values
     *
     * @return bool
     */
    public function editOrderedLecture ($id, ArrayHash $values)
    {
        return (bool)$this
            ->getById($id, false)
            ->update($values);
    }
}
