<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Utils\ArrayHash;

class OrderedCourseModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'objednane_kurzy';

    const TABLE_ENROLLED = 'prihlaseni';

    /**
     * @param int|null $course_id
     * @param int|null|0 $ico
     *
     * @return Selection
     */
    public function getOrderedCourses ($course_id = null, $ico = null)
    {
        $courses = $this
            ->getTable();

        if ($course_id) {
            $courses = $courses->where('kurz', $course_id);
        }

        if ($ico !== 0) {
            $courses = $courses->where('firma IS NULL OR firma', $ico);    // veřejné nebo od firmy
        }

        return $courses;
    }

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getOrderedCourse ($id)
    {
        return $this
            ->getTable()
            ->wherePrimary($id)
            ->fetch();
    }

    /**
     * @param int $ico
     *
     * @return Selection
     */
    public function getOrderedCoursesByCompany ($ico)
    {
        return $this
            ->getAll()
            ->where('firma', $ico);
    }

    /**
     * @param ArrayHash $values
     *
     * @return bool|int|IRow
     */
    public function orderCourse (ArrayHash $values)
    {
        return $this
            ->getTable()
            ->insert($values);
    }

    /**
     * @param int $course_id
     * @param ArrayHash $data
     *
     * @return int
     */
    public function editOrderedCourse ($course_id, ArrayHash $data)
    {
        return $this
            ->getTable()
            ->wherePrimary($course_id)
            ->update($data);
    }

    /**
     * @param $id_attendee
     * @param $id_course
     *
     * @return bool
     */
    public function enroll ($id_attendee, $id_course)
    {
        try {
            return (bool)$this
                ->getTable(self::TABLE_ENROLLED)
                ->insert(array (
                             'rc'   => $id_attendee,
                             'kurz' => $id_course,
                         ));
        } catch (UniqueConstraintViolationException $e) {
            return false;
        }
    }

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getByLecture ($id)
    {
        return $this
            ->getTable()
            ->where(':objednane_lekce.id', $id)
            ->fetch();
    }
}
