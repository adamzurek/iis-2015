<?php

namespace App\Model;

use Nette\Security\IAuthorizator;
use Nette\Security\Permission;

class UserAuthorizator extends Permission implements IAuthorizator
{
    public function __construct ()
    {
        $logged = array (
            'firma',
            'stredisko',
            'ucastnik',
            'zamestnanec',
        );

        $all = array_merge($logged, array ('guest'));

        $this->addRoles();
        $this->addResoures();

        /* verejna cast */
        parent::allow('guest', 'Attendee', 'register');
        parent::allow('guest', 'Company', 'register');
        parent::allow($all, array ('Course', 'OrderedCourse'), 'list');
        parent::allow($all, 'Homepage', 'default');
        parent::allow($all, 'Room', 'list');
        parent::allow($all, 'Room', 'timetable');
        parent::allow('guest', 'Sign', 'in');
        parent::allow('guest', 'Sign', 'register');

        /* zaheslovana cast */
        parent::allow('firma', 'Course', 'order');
        parent::allow('firma', 'OrderedCourse', 'listCompanyOrdered');
        parent::allow('firma', 'OrderedCourse', 'list');
        parent::allow('firma', 'Company', 'edit');

        parent::allow('stredisko', 'Attendee', array ('edit', 'list'));
        parent::allow('stredisko', 'Company', 'edit');
        parent::allow('stredisko', 'Course', array ('add', 'edit', 'list'));
        parent::allow('stredisko', 'Employee');
        parent::allow('stredisko', 'Lecture', array ('edit', 'order'));
        parent::allow('stredisko', 'OrderedCourse', array ('certify', 'edit', 'list', 'listAttendees', 'printCertify'));
        parent::allow('stredisko', 'OrderedLecture', array ('list', 'order'));
        parent::allow('stredisko', 'Room', 'add');
        parent::allow('stredisko', 'Sign', 'out');

        parent::allow('ucastnik', 'Attendee', 'edit');
        parent::allow('ucastnik', 'OrderedCourse', 'enroll');
        parent::allow('ucastnik', 'OrderedCourse', 'list');

        parent::allow('zamestnanec', 'OrderedCourse', array ('edit', 'list', 'listAttendees'));

        parent::allow($logged, 'Lecture', 'list');
        parent::allow($logged, 'Sign', 'out');
    }

    private function addRoles ()
    {
        parent::addRole('firma');
        parent::addRole('guest');
        parent::addRole('stredisko');
        parent::addRole('ucastnik');
        parent::addRole('zamestnanec');
    }

    private function addResoures ()
    {
        parent::addResource('Attendee');
        parent::addResource('Course');
        parent::addResource('Company');
        parent::addResource('Employee');
        parent::addResource('Homepage');
        parent::addResource('Lecture');
        parent::addResource('OrderedCourse');
        parent::addResource('OrderedLecture');
        parent::addResource('Room');
        parent::addResource('Sign');
    }
}
