<?php

namespace App\Model;

use Nette;
use Nette\Database\Context as DbContext;
use Nette\Http\Request;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;

class UserManager extends BaseModel implements IAuthenticator
{
    const COLUMN_PASSWORD_HASH = 'heslo';

    /**
     * @var Request
     */
    private $httpRequest;

    /**
     * @param DbContext $db
     * @param Request $httpRequest
     */
    public function __construct (DbContext $db, Request $httpRequest)
    {
        parent::__construct($db);

        $this->httpRequest = $httpRequest;
    }

    /**
     * @param array $credentials
     *
     * @return Identity
     * @throws AuthenticationException
     */
    public function authenticate (array $credentials)
    {
        list($id, $password, $role) = $credentials;

        $row = $this
            ->getTable($role)
            ->where($role === 'firma' ? 'ico' : 'rc', $id)
            ->fetch();

        if (!$row) {
            throw new AuthenticationException(
                ($role === 'firma' ? 'IČO' : 'RČ') .
                ' nenalezeno v databázi', self::IDENTITY_NOT_FOUND);
        } elseif (sha1($password) != $row[self::COLUMN_PASSWORD_HASH]) {
            throw new AuthenticationException('Zadáno špatné heslo.', self::INVALID_CREDENTIAL);
        } elseif ($row['aktivni'] != 1) {
            throw new AuthenticationException('Účet není aktivní', self::IDENTITY_NOT_FOUND);
        }

        $arr = array (
            'rc'  => $role === 'firma' ? null : $row->rc,
            'ico' => $role === 'firma'
                ? $row->ico
                : ($role === 'ucastnik' ? $row->zamestnan : null),
        );
        unset($arr[self::COLUMN_PASSWORD_HASH]);

        $securityArr = array (
            'ip'      => $this->httpRequest->getRemoteAddress(),
            'browser' => $this->httpRequest->getHeader('user-agent'),
        );

        return new Identity($id, $role, $arr + $securityArr);
    }
}
