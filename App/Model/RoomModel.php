<?php

namespace App\Model;

use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

class RoomModel extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'mistnost';

    /**
     * @return Selection
     */
    public function getRooms ()
    {
        return $this->getAll();
    }

    /**
     * @param int $id
     *
     * @return bool|IRow
     */
    public function getRoom ($id)
    {
        return $this->getById($id);
    }

    /**
     * @param int $room_id
     *
     * @return Selection
     */
    public function getTimetable ($room_id)
    {
        return $this->getById($room_id, false)
            ->order(':objednane_kurzy:objednane_lekce.datum DESC');
    }

    /**
     * @param ArrayHash $values
     *
     * @return bool
     */
    public function addRoom (ArrayHash $values)
    {
        return (bool)$this
            ->getTable()
            ->insert($values);
    }

    /**
     * @param $id
     * @param ArrayHash $values
     *
     * @return bool
     */
    public function editRoom ($id, ArrayHash $values)
    {
        return (bool)$this
            ->getById($id, false)
            ->update($values);
    }
}
