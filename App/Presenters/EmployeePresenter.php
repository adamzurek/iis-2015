<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Components\MenuControl;
use App\Model\Facade\EmployeeFacade;
use Nette\Application\BadRequestException;
use Nette\Database\Table\IRow;

class EmployeePresenter extends BasePresenter
{
    /**
     * @var EmployeeFacade
     * @inject
     * @internal
     */
    public $employeeFacade;

    /**
     * @var IRow
     */
    private $employee;

    public function beforeRender ()
    {
        parent::beforeRender();

        $this['menu']
            ->addItem(':Employee:register', 'Přidat lektora', 'plus', MenuControl::TYPE_SUB);
    }

    public function renderList ()
    {
        $this->template->employees = $this->employeeFacade->getEmployees();
    }

    /**
     * @return Form
     */
    public function createComponentRegisterForm ()
    {
        $form = $this->employeeFacade->createRegisterForm();

        $presenter = $this;
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace zaměstnance selhala.', 'danger');
        };

        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace proběhla úspěšně.', 'success');
            $presenter->redirect('list');
        };

        return $form;
    }

    /**
     * @param int $rc
     *
     * @throws BadRequestException
     */
    public function actionEdit ($rc)
    {
        $employee = $this->employeeFacade->getEmployee($rc);

        if (!$employee) {
            $this->flashMessage('Zaměstnanec nenalezen.');
            $this->redirect('list');
        }

        $this->employee = $employee;
    }

    /**
     * @return Form
     */
    public function createComponentEditForm ()
    {
        $form = $this->employeeFacade->createEditForm($this->employee);

        $presenter = $this;
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Úprava zaměstnance selhala.', 'danger');
        };

        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Úprava záznamu proběhla úspěšně.', 'success');
            $presenter->redirect('list');
        };

        return $form;
    }
}
