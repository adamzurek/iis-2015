<?php

namespace App\Presenters;

use App\Form\LectureOrderFormFactory;
use App\Model\CourseModel;
use App\Model\LectureModel;
use App\Model\OrderedCourseModel;
use App\Model\OrderedLectureModel;
use App\Components\BootstrapForm as Form;
use Nette\Utils\ArrayHash;

class OrderedLecturePresenter extends BasePresenter
{
    /**
     * @var CourseModel
     * @inject
     * @internal
     */
    public $courseModel;

    /**
     * @var OrderedCourseModel
     * @inject
     * @internal
     */
    public $orderedCourseModel;

    /**
     * @var LectureModel
     * @inject
     * @internal
     */
    public $lectureModel;

    /**
     * @var OrderedLectureModel
     * @inject
     * @internal
     */
    public $orderedLectureModel;

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=edit)
     */
    public function actionEdit ($id)
    {
        $lecture = $this->orderedLectureModel->getOrderedLecture($id);
        if (!$lecture) {
            $this->flashMessage('Lekce nenalezena.', 'danger');
            $this->redirect('list');
        }

        $this['editForm']->setDefaults($lecture);
    }

    /**
     * @param int $id_course
     * @Allowed(resource=OrderedLecture, privilege=list)
     */
    public function actionList ($id_course)
    {
        $o_course = $this->orderedCourseModel->getOrderedCourse($id_course);

        if (!$o_course) {
            $this->flashMessage('Kurz nenalezen.', 'danger');
            $this->redirect('Course:list');
        }

        $this->template->course = $this->courseModel->getCourse($o_course->kurz);
        $this->template->o_course = $o_course;
        $this->template->lectures = $this->lectureModel->getLectures($o_course->kurz);
        $this->template->o_lectures = $this->orderedLectureModel->getOrderedLectures($o_course->id)->fetchPairs('lekce');
    }

    /**
     * @param int $id
     * @param int $id_course
     * @Allowed(resource=OrderedLecture, privilege=order)
     */
    public function actionOrder ($id, $id_course)
    {
        if (!$id || !$id_course) {
            $this->flashMessage('Špatný požadavek.', 'warning');
            $this->redirect('Lecture:list');
        }

        $lecture = $this->lectureModel->getLecture($id);
        if (!$lecture) {
            $this->flashMessage('Lekce nebyla nalezena.', 'danger');
            $this->redirect('list');
        }

        $this['orderForm']['lekce']->setValue($id);
        $this['orderForm']['kurz']->setValue($id_course);
    }

    /**
     * @var LectureOrderFormFactory
     * @inject
     * @internal
     */
    public $lectureOrderFormFactory;

    /**
     * @return Form
     */
    protected function createComponentEditForm ()
    {
        $form = $this->lectureOrderFormFactory->create();

        $form->addHidden('id');

        $me = $this;

        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($me) {
            $id = $values->id;
            unset($values->id);

            $ok = $me->orderedLectureModel->editOrderedLecture($id, $values);

            $o_course = $me->orderedCourseModel->getByLecture($id);

            if ($ok) {
                $me->flashMessage('Editace proběhla úspěšně.', 'success');
                $me->redirect(302, 'OrderedCourse:list', array ('id' => $o_course->kurz));
            } else {
                $me->flashMessage('Editace selhala.', 'danger');
            }
        };

        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentOrderForm ()
    {
        $form = $this->lectureOrderFormFactory->create();

        $presenter = $this;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter) {
            return (bool)$presenter->orderedLectureModel->orderLecture($values);
        };

        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter) {
            $presenter->flashMessage('Objednání lekce proběhlo úspěšně.', 'success');
            $presenter->redirect(302, 'OrderedCourse:list', array ('id' => $values->kurz));
        };

        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Objednání lekce selhalo.', 'danger');
        };

        return $form;
    }
}
