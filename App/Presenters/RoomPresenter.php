<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Components\MenuControl;
use App\Form\RoomFormFactory;
use App\Model\RoomModel;
use Nette\Database\Table\IRow;
use Nette\Utils\ArrayHash;

class RoomPresenter extends BasePresenter
{
    /**
     * @var RoomModel
     * @inject
     * @internal
     */
    public $roomModel;

    /**
     * @var IRow
     */
    private $room;

    public function beforeRender ()
    {
        parent::beforeRender();

        $this['menu']
            ->addItem(':Room:add', 'Přidat místnost', 'plus', MenuControl::TYPE_SUB);
    }

    /**
     * @param int $id
     * @Allowed(resource=Room, privilege=timetable)
     */
    public function actionTimetable ($id)
    {
        $this->room = $room = $this->roomModel->getRoom($id);
        if (!$room) {
            $this->redirect('list');
        }
    }

    /**
     * @param int $id
     * @Allowed(resource=Room, privilege=edit)
     */
    public function actionEdit ($id)
    {
        $room = $this->roomModel->getRoom($id);
        if (!$room) {
            $this->flashMessage('Místnost nenalezena.', 'danger');
            $this->redirect('list');
        }

        $this->room = $room;
    }

    /**
     * @param int $id
     */
    public function renderTimetable ($id)
    {
        $this->template->room = $this->room;

        $this->template->timetable = $this->roomModel->getTimetable($id);
    }

    /**
     * @Allowed(resource=Room, privilege=list)
     */
    public function actionList ()
    {
        $this->template->rooms = $this->roomModel->getRooms();
    }

    /**
     * @var RoomFormFactory
     * @inject
     * @internal
     */
    public $roomFormFactory;

    /**
     * @return Form
     */
    protected function createComponentAddForm ()
    {
        $form = $this->roomFormFactory->create();

        $presenter = $this;
        $roomModel = $this->roomModel;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter, $roomModel) {
            $ok = $roomModel->addRoom($values);
            if ($ok) {
                $presenter->flashMessage('Přidání místnosti úspěšné.', 'success');
                $presenter->redirect('list');
            } else {
                $presenter->flashMessage('Přidání místnosti selhalo', 'danger');
            }
        };

        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentEditForm ()
    {
        $form = $this->roomFormFactory->create();

        $form->addHidden('id');

        $form->setDefaults($this->room);

        $presenter = $this;
        $roomModel = $this->roomModel;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter, $roomModel) {
            $id = $values->id;
            unset($values->id);

            $ok = $roomModel->editRoom($id, $values);
            if ($ok) {
                $presenter->flashMessage('Editace místnosti byla úspěšná.', 'success');
                $presenter->redirect('timetable');
            } else {
                $presenter->flashMessage('Editace byla neúspěšná.', 'danger');
            }
        };

        return $form;
    }
}
