<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Form\LectureFormFactory;
use App\Model\CourseModel;
use App\Model\LectureModel;
use Nette\Utils\ArrayHash;

class LecturePresenter extends BasePresenter
{
    /**
     * @var LectureModel
     * @inject
     * @internal
     */
    public $lectureModel;

    /**
     * @var CourseModel
     * @inject
     * @internal
     */
    public $courseModel;

    /**
     * @var LectureFormFactory
     * @inject
     * @internal
     */
    public $lectureFormFactory;

    public function beforeRender ()
    {
        parent::beforeRender();
    }

    /**
     * @param int $id
     * @Allowed(resource=Lecture, privilege=list)
     */
    public function actionList ($id)
    {
        $course = $this->courseModel->getCourse($id);
        if (!$course) {
            $this->flashMessage('Kurz nebyl nalezen', 'danger');
            $this->redirect('Course:list');
        }

        $this->template->course = $course;

        $this->template->lectures = $this->lectureModel->getLectures($id);
    }

    /**
     * @param int $id id lekce
     * @Allowed(resource=Lecture, privilege=edit)
     */
    public function actionEdit ($id)
    {
        $defaults = $this->lectureModel->getLecture($id);
        if (!$defaults) {
            $this->flashMessage('Lekce nebyla nalezena.', 'danger');
            $this->redirect('Course:list');
        }

        $this['editForm']->setDefaults($defaults);
    }

    /**
     * @return Form
     */
    public function createComponentEditForm ()
    {
        $form = $this->lectureFormFactory->create();

        $form->addHidden('id');

        $form->onSuccess[] = $this->editFormSuccess;

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editFormSuccess (Form $form, ArrayHash $values)
    {
        $id = $values->id;
        unset($values->id);

        $ok = $this->lectureModel->editLecture($id, $values);
        if ($ok) {
            $this->flashMessage('Lekce byla úspěšně upravena.', 'success');
            $this->redirect(302, 'list', array ('id' => $values->kurz));
        } else {
            $this->flashMessage('Úprava lekce selhala..', 'danger');
        }
    }

    /**
     * @return Form
     */
    public function createComponentAddForm ()
    {
        $form = $this->lectureFormFactory->create();

        $form->onSuccess[] = $this->addFormSuccess;

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function addFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->lectureModel->addLecture($values);

        if ($ok) {
            $this->flashMessage('Lekce byla vytvořena.', 'success');
            $this->redirect(302, 'list', array ('id' => $values->kurz));
        } else {
            $this->flashMessage('Vytvoření lekce selhalo.', 'danger');
        }
    }
}
