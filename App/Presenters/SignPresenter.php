<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use Nette;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;

class SignPresenter extends BasePresenter
{
    /**
     * @return Form
     */
    protected function createComponentSignInForm ()
    {
        $form = new Form;

        $form->addText('username', 'Uživatelské jméno')
            ->setRequired('Uživatelské jméno je povinnné.')
            ->getControlPrototype()->setAutocomplete('off');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Heslo je povinné.')
            ->getControlPrototype()->setAutocomplete('off');

        $form->addSelect('role', 'Role')
            ->setRequired('Zvolte způsob přihlášení.')
            ->setItems(array (
                           'firma'       => 'Firma',
                           'ucastnik'    => 'Účastník',
                           'zamestnanec' => 'Zaměstnanec',
                           'stredisko'   => 'Středisko',
                       ));

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = $this->signInFormSucceeded;

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function signInFormSucceeded (Form $form, ArrayHash $values)
    {
        $this->getUser()->setExpiration('10 minutes', true);

        try {
            $this->getUser()->login($values->username, $values->password, $values->role);

            $this->flashMessage('Přihlášení proběhlo úspěšně');
            $this->redirect('Homepage:');
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

    /**
     * @logged
     */
    public function actionOut ()
    {
        $this->getUser()->logout();

        $this->flashMessage('Odhlášeno.');

        $this->redirect('Homepage:');
    }
}
