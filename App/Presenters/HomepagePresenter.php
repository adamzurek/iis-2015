<?php

namespace App\Presenters;

class HomepagePresenter extends BasePresenter
{

    public function actionSnippet ()
    {
        $this->template->rand = rand(0, 9999);
    }

    public function handleRand ()
    {
        $this->redrawControl('s');
    }

}
