<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Model\Facade\CompanyFacade;

class CompanyPresenter extends BasePresenter
{
    /**
     * @var CompanyFacade
     * @inject
     * @internal
     */
    public $companyFacade;

    /**
     * @var int
     */
    private $company_id;

    /**
     * @param int|null $id
     * @Allowed(resource=Company, privilege=edit)
     */
    public function actionEdit ($id = null)
    {
        if ($id && $this->getUser()->isInRole('stredisko')) {
            $this->company_id = $id;
        } else {
            $this->company_id = $this->getUser()->getId();
        }
    }

    /**
     * @Allowed(resource=Company, privilege=list)
     */
    public function renderList ()
    {
        $this->template->companies = $this->companyFacade->getCompanies();
    }

    /**
     * @Allowed(resource=Company, privilege=register)
     */
    public function actionRegister ()
    {

    }

    public function createComponentEditForm ()
    {
        $form = $this->companyFacade->createEditForm($this->company_id);

        $presenter = $this;

        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Editace byla úspěšná.', 'success');
            $presenter->redirect('Homepage:');
        };
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Editace se nezdařila.', 'danger');
        };

        return $form;
    }

    /**
     * @return Form
     */
    public function createComponentRegisterForm ()
    {
        $form = $this->companyFacade->createRegisterForm();

        $presenter = $this;
        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace byla úspěšná.', 'success');
            $presenter->redirect('Homepage:');
        };
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace selhala.', 'danger');
        };

        return $form;
    }
}
