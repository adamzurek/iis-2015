<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Form\CourseOrderFormFactory;
use App\Model\AttendeeModel;
use App\Model\CompanyModel;
use App\Model\CourseModel;
use App\Model\OrderedCourseModel;
use Nette\Database\Table\IRow;
use Nette\Utils\ArrayHash;

class OrderedCoursePresenter extends BasePresenter
{
    /**
     * @var AttendeeModel
     * @inject
     * @internal
     */
    public $attendeeModel;

    /**
     * @var CourseModel
     * @inject
     * @internal
     */
    public $courseModel;

    /**
     * @var OrderedCourseModel
     * @inject
     * @internal
     */
    public $orderedCourseModel;

    /**
     * @var CompanyModel
     * @inject
     * @internal
     */
    public $companyModel;

    /**
     * @var IRow
     */
    private $attendee;

    /**
     * @var IRow
     */
    private $course;

    /**
     * @var IRow
     */
    private $o_course;

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=list)
     */
    public function actionList ($id)
    {
        $this->template->course_c = $this->courseModel->getCourse($id);

        if (in_array('zamestnanec', $this->getUser()->getRoles(), true)) {
            $this->template->courses = $this->orderedCourseModel->getOrderedCourses($id, 0);
        } else {
            $this->template->courses = $this->orderedCourseModel->getOrderedCourses($id, $this->getUser()->getIdentity()->data['ico']);
        }
    }

    /**
     * @param int $id
     */
    public function renderListAttendees ($id)
    {
        $this->template->course = $this->course;

        $this->template->attendees = $this->attendeeModel->getAttendeesByCourse($id);
    }

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=enroll)
     */
    public function actionEnroll ($id)
    {
        $course = $this->orderedCourseModel->getOrderedCourse($id);
        if (!$course) {
            $this->flashMessage('Kurz nenalezen.');
            $this->redirect('list');
        }

        if ($this->orderedCourseModel->enroll($this->getUser()->getId(), $id)) {
            $this->flashMessage('Úspěšně přihlášeno na kurz.');
        } else {
            $this->flashMessage('Nepodařilo se přihlásit na kurz.', 'danger');
        }

        $this->redirect('list');
    }

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=certify)
     */
    public function actionCertify ($id)
    {
        $this->o_course = $o_course = $this->orderedCourseModel->getOrderedCourse($id);

        if (!$o_course) {
            $this->flashMessage('Kurz nebyl nalezen.');
            $this->redirect('list');
        }

        $this->course = $course = $this->courseModel->getCourse($o_course->kurz);
    }

    /**
     * @param int $id
     */
    public function renderCertify ($id)
    {
        $this->template->o_course = $this->o_course;
        $this->template->course = $this->course;

        $this->template->attendees = $this->attendeeModel->getAttendeesByCourse($id);
    }

    /**
     * @param int $id
     * @param int $id_attendee
     * @Allowed(resource=OrderedCourse, privilege=printCertify)
     */
    public function actionPrintCertify ($id, $id_attendee)
    {
        $this->course = $course = $this->courseModel->getOrderedCourseInfo($id, $id_attendee);
        if (!$course) {
            $this->flashMessage('Kurz nebyl nalezen.');
            $this->redirect('list');
        }

        $this->attendee = $attendee = $this->attendeeModel->getAttendee($id_attendee);
        if (!$attendee) {
            $this->flashMessage('Účastník nebyl nalezen.');
            $this->redirect('list');
        }

        if (!$this->attendeeModel->hasAttendeeCoursed($id_attendee, $id)) {
            $this->flashMessage('Účastník tento kurz nenavštěvoval.');
            $this->redirect('list');
        }
    }

    public function renderPrintCertify ()
    {
        $this->template->course = $this->course;
        $this->template->attendee = $this->attendee;
    }

    /**
     * @Allowed(resource=OrderedCourse, privilege=listCompanyOrdered)
     */
    public function actionListCompanyOrdered ()
    {
        $this->template->company = $this->companyModel->getCompany($this->getUser()->getIdentity()->data['ico']);

        $this->template->courses = $this->orderedCourseModel->getOrderedCoursesByCompany($this->getUser()->getIdentity()->data['ico']);
    }

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=edit)
     */
    public function actionEdit ($id)
    {
        $course = $this->orderedCourseModel->getOrderedCourse($id);
        if (!$course) {
            $this->flashMessage('Kurz nebyl nalezen.', 'danger');
            $this->redirect('Course:list');
        }

        $this['orderEditForm']['kurz']->setValue($id);
        $this['orderEditForm']->setDefaults($course);
    }

    /**
     * @param int $id
     * @Allowed(resource=OrderedCourse, privilege=listAttendees)
     */
    public function actionListAttendees ($id)
    {
        $this->course = $course = $this->courseModel->getOrderedCourseInfo($id);

        if (!$course) {
            $this->flashMessage('Tento kurz nemáte objednaný.', 'danger');
            $this->redirect('list');
        }
    }

    /**
     * @var CourseOrderFormFactory
     * @inject
     * @internal
     */
    public $orderedCourseFormFactory;

    /**
     * @return Form
     */
    protected function createComponentOrderEditForm ()
    {
        $form = $this->orderedCourseFormFactory->create();

        $presenter = $this;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter) {
            $kurz = $values->kurz;
            unset($values->kurz);

            $ok = $presenter->orderedCourseModel->editOrderedCourse($kurz, $values);
            if ($ok) {
                $presenter->flashMessage('Editace proběhla úspěšně.', 'success');
                $presenter->redirect('OrderedCourse:list', array ('id' => $kurz));
            } else {
                $presenter->flashMessage('Editace skončila chybou nebo jste nezměnili žádnou položku.', 'danger');
            }
        };

        return $form;
    }
}
