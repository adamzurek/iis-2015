<?php

namespace App\Presenters;

use App\Components\IMenuControlFactory;
use App\Components\MenuControl;
use App\Model;
use Nette;
use Nette\Application\UI\Presenter;
use Nette\Reflection\ClassType as ReflectionClass;
use Nette\Reflection\Method as ReflectionMethod;
use WebLoader\Nette\LoaderFactory;

abstract class BasePresenter extends Presenter
{
    /**
     * @var IMenuControlFactory
     * @inject
     */
    public $menuControlFactory;

    /**
     * @return MenuControl
     */
    public function createComponentMenu ()
    {
        $menu = $this->menuControlFactory->create();

        $user = $this->getUser();
        $menu->onAttached[] = function (MenuControl $menu) use ($user) {
            $menu->addItem(':Course:list', 'Kurzy', 'menu-hamburger')
                ->addItem(':Company:register', 'Registrace firmy', 'pencil')
                ->addItem(':Attendee:register', 'Registrace účastníka', 'pencil')
                ->addItem(':Sign:in', 'Přihlásit se', 'user', MenuControl::TYPE_RIGHT);

            // potrebny login
            $menu->addItem(':Room:list', 'Místnosti', 'blackboard')
                ->addItem(':Employee:list', 'Lektoři', 'user')
                ->addItem(':Company:list', 'Firmy', 'briefcase')
                ->addItem(':Attendee:list', 'Účastníci', 'education')
                ->addItem(':Sign:out', 'Odhlásit', 'off', MenuControl::TYPE_RIGHT);

            if ($user->isInRole('firma')) {
                $menu->addItem(':Company:edit', 'Změnit údaje', 'user');
            }

            if ($user->isInRole('ucastnik')) {
                $menu->addItem(':Attendee:edit', 'Změnit údaje', 'user');
            }
        };

        return $menu;
    }

    public function beforeRender ()
    {
        $this->template->siteName = $this->context->parameters['siteName'];
    }

    /**
     * @var LoaderFactory
     * @inject
     */
    public $webLoaderFactory;

    /**
     * @return \WebLoader\Nette\CssLoader
     */
    protected function createComponentCss ()
    {
        return $this->webLoaderFactory->createCssLoader('default');
    }

    /**
     * @return \WebLoader\Nette\JavaScriptLoader
     */
    protected function createComponentJs ()
    {
        return $this->webLoaderFactory->createJavaScriptLoader('default');
    }

    protected function startup ()
    {
        $this->getUser()->getStorage()->setNamespace('iis15/back');

        parent::startup();
    }

    /**
     * @var $element
     * @forwards Homepage:|Error:blank
     */
    public function checkRequirements ($element)
    {
        $this->checkAuthorization($element);

        /*
         * ochrana před změnou ip a prohlížeče
         */
        if ($this->getUser()->isLoggedIn()) {
            if ($this->getUser()->getIdentity()->ip !== $this->getHttpRequest()->getRemoteAddress()) {
                $this->redirect(302, ':Sign:in');
            }

            if ($this->getUser()->getIdentity()->browser !== $this->getHttpRequest()->getHeader('user-agent')) {
                $this->redirect(302, ':Sign:in');
            }
        }
    }

    /**
     * @param ReflectionClass|ReflectionMethod $i
     */
    private function checkAuthorization ($i)
    {
        $fail = false;

        if ($i->hasAnnotation('logged')) {
            if (!$this->getUser()->isLoggedIn()) {
                $this->flashMessage('Pro tuto akci musíte být přihlášen(a).', 'info');
                $fail = true;
            }
        } elseif ($i->hasAnnotation('only_guest')) {
            if ($this->getUser()->isLoggedIn()) {
                $this->flashMessage('Pro tuto akci nesmíte být přihlášen(a).', 'warning');
                $fail = true;
            }
        }

        if ($i->hasAnnotation('Allowed')) {
            $allowed = $i->getAnnotation('Allowed');

            $fail = !$this->getUser()->isAllowed($allowed['resource'], $allowed['privilege']);

            if ($fail) {
                $this->flashMessage('Pro tuto akci nemáte oprávnění.', 'danger');
            }
        }

        if ($fail) {
            $this->redirect(':Sign:in');
        }
    }
}
