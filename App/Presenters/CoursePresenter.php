<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Components\MenuControl;
use App\Form\CourseFormFactory;
use App\Form\CourseOrderFormFactory;
use App\Model\CompanyModel;
use App\Model\CourseModel;
use App\Model\LectorModel;
use App\Model\OrderedCourseModel;
use App\Model\RoomModel;
use Nette\Database\Table\IRow;
use Nette\Utils\ArrayHash;

class CoursePresenter extends BasePresenter
{
    /**
     * @var RoomModel
     * @inject
     * @internal
     */
    public $roomModel;

    /**
     * @var CourseModel
     * @inject
     * @internal
     */
    public $courseModel;

    /**
     * @var OrderedCourseModel
     * @inject
     * @internal
     */
    public $orderedCourseModel;

    /**
     * @var CompanyModel
     * @inject
     * @internal
     */
    public $companyModel;

    /**
     * @var LectorModel
     * @inject
     * @internal
     */
    public $lectorModel;

    /**
     * @var IRow
     */
    private $course;

    public function beforeRender ()
    {
        parent::beforeRender();

        $this['menu']
            ->addItem('OrderedCourse:listCompanyOrdered', 'Objednané kurzy firmy', 'road', MenuControl::TYPE_SUB)
            ->addItem('Course:add', 'Přidat nový kurz', 'plus', MenuControl::TYPE_SUB);
    }

    public function renderList ()
    {
        $this->template->courses = $this->courseModel->getCourses();
    }

    /**
     * @param int $id
     * @Allowed(resource=Course, privilege=edit)
     */
    public function actionEdit ($id)
    {
        $course = $this->courseModel->getCourse($id);
        if (!$course) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $this['editForm']->setDefaults($course);
    }

    /**
     * @param int $id
     * @Allowed(resource=Course, privilege=order)
     */
    public function actionOrder ($id)
    {
        $course = $this->courseModel->getCourse($id);
        if (!$course) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $this['orderForm']->setDefaults($course);
        $this['orderForm']['kurz']->setValue($id);
    }

    /**
     * @return Form
     */
    public function createComponentEditForm ()
    {
        $form = $this->courseFormFactory->create();

        $form->addHidden('id');

        $presenter = $this;
        $courseModel = $this->courseModel;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter, $courseModel) {
            $id = $values->id;
            unset($values->id);

            $ok = $courseModel->editCourse($id, $values);
            if ($ok) {
                $presenter->flashMessage('Úprava záznamu proběhla úspěšně.', 'success');
                $presenter->redirect('list');
            } else {
                $presenter->flashMessage('Úprava záznamu selhala.', 'danger');
            }
        };

        return $form;
    }

    /**
     * @var CourseFormFactory
     * @inject
     * @internal
     */
    public $courseFormFactory;

    /**
     * @return Form
     */
    public function createComponentAddForm ()
    {
        $form = $this->courseFormFactory->create();

        $form->onSuccess[] = array ($this, 'addFormSuccess');

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function addFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->courseModel->addCourse($values);
        if ($ok) {
            $this->flashMessage('Vložení záznamu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Vložení záznamu selhalo.', 'danger');
        }
    }

    /**
     * @var CourseOrderFormFactory
     * @inject
     * @internal
     */
    public $orderedCourseFormFactory;

    /**
     * @return Form
     */
    protected function createComponentOrderForm ()
    {
        $form = $this->orderedCourseFormFactory->create();

        $presenter = $this;
        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($presenter) {
            $values->firma = $presenter->getUser()->getIdentity()->data['ico'];

            $ok = $presenter->orderedCourseModel->orderCourse($values);
            if ($ok) {
                $presenter->flashMessage('Objednání kurzu proběhlo úspěšně.', 'success');
                $presenter->redirect('list');
            } else {
                $presenter->flashMessage('Objednání kurzu selhalo.', 'danger');
            }
        };

        return $form;
    }
}
