<?php

namespace App\Presenters;

use App\Components\BootstrapForm as Form;
use App\Model\CompanyModel;
use App\Model\Facade\AttendeeFacade;
use Nette\Database\Table\IRow;

class AttendeePresenter extends BasePresenter
{
    /**
     * @var AttendeeFacade
     * @inject
     * @internal
     */
    public $attendeeFacade;

    /**
     * @var CompanyModel
     * @inject
     * @internaů
     */
    public $companyModel;

    /**
     * @var IRow
     */
    private $attendee;

    /**
     * @Allowed(resource=Attendee, privilege=register)
     */
    public function actionRegister ()
    {

    }

    /**
     * @Allowed(resource=Attendee, privilege=list)
     */
    public function actionList ()
    {
        $this->template->attendees = $this->attendeeFacade->getAttendees();
    }

    /**
     * @return Form
     */
    public function createComponentRegisterForm ()
    {
        $form = $this->attendeeFacade->createRegisterForm();

        $presenter = $this;
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace selhala.', 'danger');
        };
        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Registrace proběhla úspěšně.', 'success');
            $presenter->redirect('Course:list');
        };

        return $form;
    }

    /**
     * @param int|null $id
     * @Allowed(resource=Attendee, privilege=edit)
     */
    public function actionEdit ($id = null)
    {
        if ($this->getUser()->isInRole('stredisko') && $id) {
            $attendee_id = $id;
        } else {
            $attendee_id = $this->getUser()->getId();
        }

        $attendee = $this->attendeeFacade->getAttendee($attendee_id);
        if (!$attendee) {
            $this->flashMessage('Účastník nebyl nalezen.');
            $this->redirect('list');
        }

        $this->attendee = $attendee;
    }

    /**
     * @return Form
     */
    public function createComponentEditForm ()
    {
        $form = $this->attendeeFacade->createEditForm($this->attendee);

        $presenter = $this;
        $form->onError[] = function () use ($presenter) {
            $presenter->flashMessage('Editace účastníka selhala.', 'danger');
        };
        $form->onSuccess[] = function () use ($presenter) {
            $presenter->flashMessage('Editace účastníka byla úspěšná', 'success');
            $presenter->redirect('Homepage:');
        };

        return $form;
    }
}
