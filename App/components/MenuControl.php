<?php

namespace App\Components;

use Nette\Application\UI\Control;
use Nette\Application\UI\Presenter;
use Nette\Security\User;
use Nette\Utils\Strings;

/**
 * @method onAttached (MenuControl $menu)
 */
class MenuControl extends Control
{
    const TYPE_MAIN = ':main',
        TYPE_RIGHT = ':right',
        TYPE_SUB = ':sub';

    /**
     * @var callable[] (MenuControl $menu)
     */
    public $onAttached = array ();

    /**
     * @var array
     */
    private $menu = array (
        self::TYPE_MAIN  => array (),
        self::TYPE_RIGHT => array (),
        self::TYPE_SUB   => array (),
    );

    /**
     * @var string
     */
    private $brand = 'Domů';

    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct (User $user)
    {
        $this->user = $user;

        if ($user->isLoggedIn()) {
            $this->onAttached[] = function (MenuControl $menu) use ($user) {
                $menu->addItem(null, 'Přihlášen: ' . implode(', ', $user->getRoles()), '', MenuControl::TYPE_RIGHT);
            };
        }
    }

    public function render ()
    {
        $this->template->brand = $this->brand;

        $this->template->menu = $this->menu[self::TYPE_MAIN];
        $this->template->submenu = $this->menu[self::TYPE_SUB];
        $this->template->right = $this->menu[self::TYPE_RIGHT];

        $this->template->setFile(__DIR__ . '/menu.latte');
        $this->template->render();
    }

    /**
     * @param Presenter $presenter
     */
    public function attached ($presenter)
    {
        parent::attached($presenter);

        $this->onAttached($this);
    }

    /**
     * @param string|null $link
     * @param string $text
     * @param string $icon
     * @param string $type
     *
     * @return MenuControl
     */
    public function addItem ($link = null, $text, $icon = '', $type = MenuControl::TYPE_MAIN)
    {
        $startsWithColon = (int)($link[0] === ':');
        $lastColon = strrpos($link, ':');

        $action = Strings::substring($link, $lastColon + 1);
        $presenter = Strings::substring($link, $startsWithColon, $lastColon - $startsWithColon);

        if ($link === null || $this->user->isAllowed($presenter, $action)) {
            $this->menu[$type][] = (object)array (
                'link' => $link ? $this->getPresenter()->link($link) : null,
                'text' => $text,
                'icon' => $icon,
            );
        }

        return $this;
    }
}

interface IMenuControlFactory
{
    /**
     * @return MenuControl
     */
    public function create ();
}
