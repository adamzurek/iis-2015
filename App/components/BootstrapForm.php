<?php

namespace App\Components;

use Nette;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls;
use Nextras\Forms\Rendering\Bs3FormRenderer;

class BootstrapForm extends Form
{

    /**
     * @param IContainer|null $parent
     * @param null $name
     */
    public function __construct (IContainer $parent = null, $name = null)
    {
        $renderer = new Bs3FormRenderer();

        $this->setRenderer($renderer);

        $this->addProtection('Formulář je neplatný. Odešlete jej znovu.');
    }
}
